#!/usr/bin/env python3
# -*- coding: ascii -*-
"""reformat_time.py

Change date format from:

    MM/DD/YYYY HH:MM:SS am/pm

to:

    YYYY-MM-DD HH:MM:SS

in a CSV file
"""

import csv
from datetime import date
from datetime import datetime
import sys

# Abre el archivo

with open('DatosMP25.csv', 'r') as csvfile:

    # Realiza el parseo de los datos
    csvreader = csv.reader(csvfile, delimiter=';')

    # Itera las filas
    for row in csvreader:

        # Itera las columnas por cada fila
        for index, col in enumerate(row):

            # Intenta realizar el parseo de cada columna
            try:
                #Cambia el formato de la fecha
                _datetime = datetime.strptime(col, "%y%m%d")
                newcol = _datetime.strftime("%Y-%m-%d")

            # Si no se logro deja la columna sin cambios
            except ValueError:
                newcol = col

            # Actualiza el Valor de la columna
            row[index] = newcol
        #Se añade un -1 para evitar que se genere una columna nueva al pasar
        #siguiente script
        print (';'.join(row)[:-1])
