import subprocess
import os
import csv
from datetime import datetime
import psycopg2
temporal = 'Datos_MP25.csv'
final = 'DatosMP5.csv'

#Se ejecuta el codigo para descargar los datos meteorologicos
exec(open("descargar_meteorologicos.py").read())

#realiza la descarga de los datos MP2.5 y realiza una primera modificación del archivo
#y genera el archivo de salida Datos_MP25.csv 
with open(temporal, "w+") as output:
    subprocess.call(["python3","./descargar_MP25.py"]);
    subprocess.call(["python3","./cambiar.py"], stdout=output);
#Utilizando el archivo generado en el paso anterior se realiza una nueva modificación 
#para poder dejar en un formato correcto los datos y guardando la salida en el archivo DatosMP25.csv
with open(final, "w+") as output:
    subprocess.call(["python3","./cambiar2.py"], stdout=output);

#Se realiza la conexion a la base de datos
conn = psycopg2.connect(
    host="localhost",
    database="ej",
    user="postgres",
    password="andres")
cur = conn.cursor()
#Abre el archivo DatosMP25.csv para poder realizar la insercion de estos.
with open(final,'r') as f:
    next(f)
    cur.copy_from(f,'Datos_MP25', sep=';')
    conn.commit()
    conn.close()
#Cierra el Archivo    
f.close()
