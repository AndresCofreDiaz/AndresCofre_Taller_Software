# -*- coding: utf-8 -*-
from datetime import datetime
import xml.etree.ElementTree as ET
import os
import psycopg2
#se generan los datos del link
fecha = datetime.today().strftime('%Y-%m-%d')
id_ema = "142"
data_file = "data.xml"#-" + fecha + ".txt"
fecha1= "2019-01-01"
#Se almacena en cmd el comando wget completo
cmd = 'wget --no-check-certificate '
cmd += '"https://www.agromet.cl/ext/aux/getGraphData.php?'
cmd += 'ema_ia_id=' + id_ema
cmd += '&dateFrom='+ fecha1  +'%2000:00:00'
cmd += '&dateTo=' + fecha + '%2023:00:00'
cmd += '&portada=false"'
cmd += ' -O ' + data_file
#Se ejecuta el comando almacenado en cmd
os.system(cmd)
#Se inicia el parseo del archivo xml
tree = ET.parse('data.xml')
root = tree.getroot()
myfile = open('Datos_Meteorologicos.csv', 'w')
for dato in root.findall('dato'):
        #print(dato.text)
        myfile.write("%s\n" % dato.text)

myfile.close()



with open('Datos_Meteorologicos.csv', 'r') as file :
  filedata = file.read()

# Modifica el archivo para dejar este en el formato correcto
filedata = filedata.replace('fecha', '')#Se elimina fecha del archivo
filedata = filedata.replace('|1|', ',') #Se realiza un reemplazo por comas de las variables
filedata = filedata.replace('|2|', ',') #que sean numeros entre pipline incluido este
filedata = filedata.replace('|3|', ',')
filedata = filedata.replace('|4|', ',')
filedata = filedata.replace('|5|', ',')
filedata = filedata.replace('|6|', ',')
filedata = filedata.replace('|7|', ',')
filedata = filedata.replace('|8|', ',')
filedata = filedata.replace('|9|', ',')
filedata = filedata.replace('|11|', ',')
filedata = filedata.replace('|12|', ',')
filedata = filedata.replace('|', '') #El primer pipline es eliminado
filedata = filedata.replace(' ', ',')# Queda un espacio entre fecha y hora el cual reemplaza por una coma

#Guarda los cambios en el archivo
with open('Datos_Meteorologicos.csv', 'w') as file:
  file.write(filedata)
#Realiza conexion con la base de datos
conn = psycopg2.connect(
    host="localhost",
    database="ej",
    user="postgres",
    password="andres")
cur = conn.cursor()
#Lee el archivo para realizar la insercion de este en la Tabla
with open('Datos_Meteorologicos.csv','r') as f:
    cur.copy_from(f,'datos2', sep=',')
    conn.commit()
    #se cierra la conexion
    conn.close()
#Se cierra el archivo    
f.close()
