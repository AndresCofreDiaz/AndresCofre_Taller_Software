import csv
from datetime import date
from datetime import datetime
import sys
#Abre el archivo
with open('Datos_MP25.csv', 'r') as csvfile:

    # Parsea los datos
    csvreader = csv.reader(csvfile, delimiter=';')

    # Itera las filas
    for row in csvreader:

        # Itera las columnas
        for index, col in enumerate(row):

            # Intenta parsear y convertir cada columna
            if (index ==1):
                try:
                    #Modifica el dato de la Hora
                    _datetime = datetime.strptime(col, "%H%M")
                    newcol = _datetime.strftime("%H:%M:%S")

                # Si no se logra deja la columna sin cambios
                except ValueError:
                    newcol = col

                # Actualiza el valor de la columna
                row[index] = newcol
        print (';'.join(row))
