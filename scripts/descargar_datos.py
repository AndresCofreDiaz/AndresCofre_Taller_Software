import pycurl
from io import BytesIO

for ANIO in range(2000, 2021):

    DATE_START = f"{ANIO}-01-01"
    # DATE_END = f"{ANIO}-10-21"  # Cambiar a fecha de anio actual
    DATE_END = f"{ANIO}-12-31"
    EMA_ID = 143

    # ANIO = 2021
    URL = f"https://www.agromet.cl/ext/aux/getGraphData.php?ema_ia_id={EMA_ID}&dateFrom={DATE_START}%2000:00:00&dateTo={DATE_END}%2000:00:00&portada=false"

    b_obj = BytesIO()
    curl = pycurl.Curl()
    # curl.setopt(pycurl.CAINFO, certifi.where())
    curl.setopt(pycurl.SSL_VERIFYHOST, 0)
    curl.setopt(pycurl.SSL_VERIFYPEER, 0)
    curl.setopt(pycurl.URL, URL)
    curl.setopt(curl.WRITEDATA, b_obj)
    curl.perform()

    curl.close()

    get_body = b_obj.getvalue()

    with open(f"{ANIO}.xml", "w") as archivo:
        archivo.write(get_body.decode('utf8'))

 # curl -k "https://www.agromet.cl/ext/aux/getGraphData.php?ema_ia_id=143&dateFrom=2000-01-01%2000:00:00&dateTo=2021-10-21%2000:00:00&portada=false"  -H "Accept: application/xml"
